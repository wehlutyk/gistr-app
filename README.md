Gistr [![Build Status](https://travis-ci.org/interpretation-experiment/gistr-app.svg?branch=master)](https://travis-ci.org/interpretation-experiment/gistr-app) [![Dependencies Status](https://david-dm.org/interpretation-experiment/gistr-app.svg)](https://david-dm.org/interpretation-experiment/gistr-app)
=====

Gistr is both a "broken telephone" (or "chinese whispers") game and an
experiment in cognitive and social science studying how we make sense and
interpret, and long range effects of that.

It is developed for my PhD on interpretation and cultural evolution.

[Dive into more information](https://github.com/interpretation-experiment/gistr-app/wiki).
